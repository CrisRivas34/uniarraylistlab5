package university.jala;

import university.jala.interfaces.Sortable;
import university.jala.interfaces.Unique;

import java.util.*;

public class UniArrayList<T> implements Sortable<T>, Unique<T>, List<T> {

  private static final int DEFAULT_CAPACITY = 0;
  private Object[] array;
  private int size;

  public UniArrayList() {
    array = new Object[DEFAULT_CAPACITY];
    size = 0;
  }

  public UniArrayList(T[] array) {
    this.array = new Object[array.length];
    for (int i = 0; i < array.length; i++) {
      this.array[i] = array[i];
    }
    this.size = array.length;
  }

  @Override
  public void sort() {
    for (int i = 0; i < size - 1; i++) {
      for (int j = 0; j < size - i - 1; j++) {
        if (compareElements((T) array[j], (T) array[j + 1]) > 0) {
          swap(j, j + 1);
        }
      }
    }
  }

  public int compareElements(T element1, T element2) {
    Comparable<T> comparable1 = (Comparable<T>) element1;
    return comparable1.compareTo(element2);
  }

  @Override
  public void sortBy(Comparator<T> comparator) {
    for (int i = 0; i < size - 1; i++) {
      for (int j = i + 1; j < size; j++) {
        T element1 = get(i);
        T element2 = get(j);

        if (comparator.compare(element1, element2) > 0) {
          swap(i, j);
        }
      }
    }
  }

  @Override
  public void unique() {
    int uniqueCount = 0;
    for (int i = 0; i < size; i++) {
      T current = (T) array[i];
      boolean isDuplicate = false;
      for (int j = 0; j < uniqueCount; j++) {
        if (array[j].equals(current)) {
          isDuplicate = true;
          break;
        }
      }
      if (!isDuplicate) {
        array[uniqueCount++] = current;
      }
    }
    for (int i = uniqueCount; i < size; i++) {
      array[i] = null;
    }
    size = uniqueCount;
  }




  @Override
  public int size() {
    return array.length;
  }

  @Override
  public boolean isEmpty() {
    return false;
  }

  @Override
  public boolean contains(Object o) {
    return false;
  }

  @Override
  public Iterator<T> iterator() {
    return new UniArrayListIterator();
  }

  private class UniArrayListIterator implements Iterator<T> {
    private int currentIndex = 0;

    @Override
    public boolean hasNext() {
      return currentIndex < size;
    }

    @Override
    public T next() {
      if (!hasNext()) {
        throw new NoSuchElementException("No more elements in the list");
      }
      return get(currentIndex++);
    }
  }


  @Override
  public Object[] toArray() {
    return new Object[0];
  }

  @Override
  public <T1> T1[] toArray(T1[] t1s) {
    return null;
  }

  @Override
  public boolean add(T t) {
    ensureCapacity();
    array[size++] = t;
    return true;
  }

  @Override
  public boolean containsAll(Collection<?> collection) {
    return false;
  }

  @Override
  public boolean addAll(Collection<? extends T> collection) {
    return false;
  }

  @Override
  public boolean addAll(int i, Collection<? extends T> collection) {
    return false;
  }

  @Override
  public boolean removeAll(Collection<?> collection) {
    return false;
  }

  @Override
  public boolean retainAll(Collection<?> collection) {
    return false;
  }

  @Override
  public void clear() {

  }

  public T get(int index) {
    if (index < 0 || index >= size) {
      throw new IndexOutOfBoundsException("Index out of range");
    }
    return (T) array[index];
  }

  @Override
  public T set(int i, T t) {
    return null;
  }

  @Override
  public void add(int index, T t) {
    if (index < 0 || index > size) {
      throw new IndexOutOfBoundsException("Index out of range");
    }

    ensureCapacity();

    for (int i = size; i > index; i--) {
      array[i] = array[i - 1];
    }

    array[index] = t;
    size++;
  }

  @Override
  public T remove(int index) {
    if (index < 0 || index >= size) {
      throw new IndexOutOfBoundsException("Index out of range");
    }

    T removedElement = get(index);
    removeElementAtIndex(index);
    return removedElement;
  }

  private void ensureCapacity() {
    if (size == array.length) {
      int newCapacity = array.length + 1000000;
      Object[] newArray = new Object[newCapacity];
      for (int i = 0; i < size; i++) {
        newArray[i] = array[i];
      }
      array = newArray;
    }
  }
  @Override
  public boolean remove(Object obj) {
    for (int i = 0; i < size; i++) {
      if (obj.equals(array[i])) {
        removeElementAtIndex(i);
        return true;
      }
    }
    return false;
  }

  private void removeElementAtIndex(int index) {
    for (int i = index; i < size - 1; i++) {
      array[i] = array[i + 1];
    }
    array[size - 1] = null;
    size--;
  }

  @Override
  public int indexOf(Object o) {
    return 0;
  }

  @Override
  public int lastIndexOf(Object o) {
    return 0;
  }

  @Override
  public ListIterator<T> listIterator() {
    return null;
  }


  @Override
  public ListIterator<T> listIterator(int i) {
    return null;
  }

  @Override
  public List<T> subList(int i, int i1) {
    return null;
  }

  private void swap(int i, int j) {
    T temp = get(i);
    array[i] = array[j];
    array[j] = temp;
  }

  @Override
  public String toString() {
    if (size == 0) {
      return "[]";
    }
    StringBuilder sb = new StringBuilder();
    sb.append("[");
    for (int i = 0; i < size - 1; i++) {
      sb.append(get(i)).append(", ");
    }
    sb.append(get(size - 1)).append("]");
    return sb.toString();
  }
}
