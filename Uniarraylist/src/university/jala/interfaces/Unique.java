package university.jala.interfaces;

public interface Unique<T> {
  void unique();
}