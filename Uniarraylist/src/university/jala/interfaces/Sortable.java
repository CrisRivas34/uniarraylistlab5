package university.jala.interfaces;

import java.util.Comparator;

public interface Sortable<T> {
  void sort();

  void sortBy(Comparator<T> comparator);
}

