package university.jala;

import java.util.Comparator;
import java.util.Objects;

public class Student implements Comparable<Student>{
  private String name;
  private int grade;

  public Student(String name, int grade) {
    this.name = name;
    this.grade = grade;
  }

  public static Comparator<Student> byGrade() {
    return new Comparator<Student>() {
      @Override
      public int compare(Student student1, Student student2) {
        return Integer.compare(student1.getGrade(), student2.getGrade());
      }
    };
  }

  public String getName() {
    return name;
  }

  public int getGrade() {
    return grade;
  }

  @Override
  public String toString() {
    return "{" + name + "," + grade + "}";
  }

  @Override
  public int compareTo(Student student) {
    return this.name.compareTo(student.name);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Student student = (Student) o;
    return grade == student.grade && Objects.equals(name, student.name);
  }
}
